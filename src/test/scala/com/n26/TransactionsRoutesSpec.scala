package com.n26

import akka.actor.ActorRef
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{ Matchers, WordSpec }

class TransactionsRoutesSpec extends WordSpec
  with Matchers
  with ScalaFutures
  with ScalatestRouteTest
  with TransactionRoutes {

  override val transactionRegistryActor: ActorRef =
    system.actorOf(TransactionRegistryActor.props, "transactionStatsServer")

  lazy val routes = transactionRoutes

  "TransactionRoutes" should {
    "return no transactions if no present (GET /statistics)" in {
      // note that there's no need for the host part in the uri:
      val request = HttpRequest(uri = "/statistics")

      request ~> routes ~> check {
        status should ===(StatusCodes.OK)

        // and no entries should be in the list:
        entityAs[String] should ===("")
      }
    }

    "be able to add users (POST /transactions)" in {
      val transaction = Transaction(42.5, "2018-07-17T09:59:51.312Z")
      val transactionsEntity = Marshal(transaction).to[MessageEntity].futureValue

      val request = Post("/transactions").withEntity(transactionsEntity)

      request ~> routes ~> check {
        status should ===(StatusCodes.Accepted)

        entityAs[String] should ===("""TransactionAdded""")
      }
    }
    "be able to remove users (DELETE /transactions)" in {
      val request = Delete(uri = "/transactions")

      request ~> routes ~> check {
        status should ===(StatusCodes.Accepted)
        entityAs[String] should ===("""TransactionsDeleted""")
      }
    }
  }
}
