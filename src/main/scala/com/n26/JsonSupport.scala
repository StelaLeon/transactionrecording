package com.n26

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport {
  import DefaultJsonProtocol._

  implicit val transactionJsonFormat = jsonFormat2(Transaction)

  implicit val transactionsJFormat = jsonFormat1(Transactions)

}
