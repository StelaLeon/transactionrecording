package com.n26

import akka.actor.{ ActorRef, ActorSystem }
import akka.event.Logging
import scala.concurrent.duration._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.delete
import akka.http.scaladsl.server.directives.MethodDirectives.get
import akka.http.scaladsl.server.directives.MethodDirectives.post
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.directives.PathDirectives.path
import scala.concurrent.Future
import akka.pattern.ask
import akka.util.Timeout
import com.n26.TransactionRegistryActor.{ ActionPerformed, AddOneTransaction, DeleteTransactions, GetTransactions }

trait TransactionRoutes extends JsonSupport {
  implicit def system: ActorSystem

  lazy val log = Logging(system, classOf[TransactionRoutes])
  def transactionRegistryActor: ActorRef

  implicit lazy val timeout = Timeout(5.seconds) // usually we'd obtain the timeout from the system's configuration

  lazy val transactionRoutes: Route =
    concat(
      path("transactions") {
        concat(
          delete {
            val transactionDeleted: Future[ActionPerformed] =
              (transactionRegistryActor ? DeleteTransactions).mapTo[ActionPerformed]
            onSuccess(transactionDeleted) { performed =>
              log.info("Transactions deleted: {}", performed.message)
              complete((StatusCodes.Accepted, performed.message))
            }
          },
          post {
            entity(as[Transaction]) { transaction =>
              val transactionAdded: Future[ActionPerformed] =
                (transactionRegistryActor ? AddOneTransaction(transaction)).mapTo[ActionPerformed]
              onSuccess(transactionAdded) { performed =>
                log.info("Transaction added transaction [{}]: {}", transaction.amount, performed.message)
                complete((StatusCodes.Accepted, performed.message))
              }
            }
          }
        )
      },

      path("statistics") {
        concat(
          get {
            val trans = (transactionRegistryActor ? GetTransactions).mapTo[Transactions]
            //rejectEmptyResponse {
            onSuccess(trans) { transactions =>
              if (transactions.transactions.isEmpty)
                complete("")
              else {
                val respJson =
                  s"""
                      "sum": ${transactions.getSum},
                      "avg" : ${transactions.getAvg},
                      "max" : ${transactions.getMax},
                      "min" : ${transactions.getMin},
                      "count" : ${transactions.getCount}
                    """.stripMargin

                complete(StatusCodes.OK, respJson)
              }

            }
            //}
          }
        )
      }
    )
}
