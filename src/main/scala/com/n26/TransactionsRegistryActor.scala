package com.n26

import akka.actor.{ Actor, ActorLogging, Props }

import scala.collection.mutable.ListBuffer

case class Transaction(amount: Double, timestamp: String)
case class Transactions(transactions: Seq[Transaction]) {

  def getMax = transactions.maxBy(_.amount).amount

  def getMin = transactions.minBy(_.amount).amount

  def getCount = transactions.size

  def getSum = transactions.foldLeft(0.0)(_ + _.amount)
  def getAvg = getSum / getCount
}

object TransactionRegistryActor {
  final case class ActionPerformed(message: String)
  final case object GetTransactions
  final case class AddOneTransaction(transaction: Transaction)
  final case object DeleteTransactions

  def props: Props = Props[TransactionRegistryActor]
}

class TransactionRegistryActor extends Actor with ActorLogging with JsonSupport {

  import TransactionRegistryActor._
  var transactions: ListBuffer[Transaction] = ListBuffer.empty[Transaction]

  def receive: Receive = {
    case GetTransactions => {
      sender() ! Transactions(transactions)
    }
    case DeleteTransactions => {
      transactions = ListBuffer.empty[Transaction]
      sender() ! ActionPerformed(s"TransactionsDeleted")
    }
    case AddOneTransaction(transaction) => {
      transactions += transaction
      sender() ! ActionPerformed("TransactionAdded")
    }
  }
}
