package com.n26

import scala.concurrent.Await
import scala.concurrent.duration.Duration

import akka.actor.{ ActorRef, ActorSystem }
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer

object QuickstartServer extends App with TransactionRoutes {

  implicit val system: ActorSystem = ActorSystem("transactionStatsServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val transactionRegistryActor: ActorRef = system.actorOf(TransactionRegistryActor.props, "transactionRegistryActor")

  lazy val routes: Route = transactionRoutes

  Http().bindAndHandle(routes, "localhost", 8080)

  println(s"Server online at http://localhost:8080/")

  Await.result(system.whenTerminated, Duration.Inf)

}

